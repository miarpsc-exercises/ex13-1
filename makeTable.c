#include <stdio.h>

char Net1[4][6] = { "QMN", "UP", "MVTN", "SEMTN" };
char Net2[4][6] = { "MACS", "GLETN", "MIDTN", "MITN" };
char Dist1[4][4] = { "1", "8", "6", "2" };
char Dist2[4][4] = { "5", "NWS", "7", "3" };
char F1a[4][8] = { "3563","3932","145470","147280" };
char F2a[4][8] = { "3932","3932","3584","3932" };
char F1b[4][8] = { "7064", "7232", "3584", "3584" };
char F2b[4][8] = { "7232", "7232", "7045", "7232" };
int nHour[4] = { 8, 10, 13, 15 };

void main( void )
{
  int i;

  printf("<?xml version='1.0' encoding='utf-8' ?>\n");
  printf("<!DOCTYPE article PUBLIC \"-//OASIS//DTD DocBook XML V4.5//EN\" \"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd\" [\n");
  printf("<!ENTITY % BOOK_ENTITIES SYSTEM \"Exercise_Outline.ent\">\n");
  printf("%BOOK_ENTITIES;\n");
  printf("]>\n\n");
  printf("<section id=\"sect-schedule\">\n");
  printf("  <title>Exercise Conduct</title>\n");
  printf("  <para>\n");
  printf("    <table>\n");
  printf("      <title>Exercise Schedule</title>\n");
  printf("      <tgroup cols=\"6\">\n");
  printf("        <colspec colnum=\"1\" colname=\"c1\" colwidth=\"4*\" />\n");
  printf("        <colspec colnum=\"2\" colname=\"c2\" colwidth=\"3*\" />\n");
  printf("        <colspec colnum=\"3\" colname=\"c3\" colwidth=\"6*\" />\n");
  printf("        <colspec colnum=\"4\" colname=\"c4\" colwidth=\"4*\" />\n");
  printf("        <colspec colnum=\"5\" colname=\"c5\" colwidth=\"4*\" />\n");
  printf("        <colspec colnum=\"6\" colname=\"c6\" colwidth=\"16*\" />\n");
  printf("        <thead>\n");
  printf("          <row>\n");
  printf("            <entry>Time</entry>\n");
  printf("            <entry>Dist</entry>\n");
  printf("            <entry>Net</entry>\n");
  printf("            <entry>Freq1</entry>\n");
  printf("            <entry>Freq2</entry>\n");
  printf("            <entry>What</entry>\n");
  printf("          </row>\n");
  printf("        </thead>\n");
  printf("        <tbody>\n");
  for ( i=0; i<4; i++ )
    {
      printf("\n<!-- %-6s %4s %8s  -  %-6s %4s %8s -->\n",
	     Net1[i],Dist1[i],F1a[i],Net2[i],Dist2[i],F2a[i]);
      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:00</entry>\n",nHour[i]);
      printf("            <entry align=\"center\">%s</entry>\n",Dist1[i]);
      printf("            <entry>%s Outbound</entry>\n",Net1[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F1a[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F1b[i]);
      printf("            <entry>%s to send a liaison to SEOC, take traffic</entry>\n",Net1[i]);
      printf("          </row>\n");
      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:10</entry>\n",nHour[i]);
      printf("            <entry align=\"center\">%s</entry>\n",Dist2[i]);
      printf("            <entry>%s Outbound</entry>\n",Net2[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F2a[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F2b[i]);
      //      if ( strcmp( Dist2[i],"NWS") )
	   printf("            <entry>%s to send a liaison to SEOC, take traffic</entry>\n",Net2[i]);
	   //      else
	   //	   printf("            <entry>%s to send a liaison to SEOC, pick up radiogram for each office</entry>\n",Net2[i]);
      printf("          </row>\n");

      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:20</entry>\n",nHour[i]);
      printf("            <entry align=\"center\">%s</entry>\n",Dist1[i]);
      printf("            <entry>counties</entry>\n");
      printf("            <entry align=\"center\">3584</entry>\n");
      printf("            <entry align=\"center\">3932</entry>\n");
      printf("            <entry>D%s counties to deliver ICS-217 directly to the SEOC</entry>\n",Dist1[i]);
      printf("          </row>\n");


      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:50</entry>\n",nHour[i]);
      printf("            <entry align=\"center\">%s</entry>\n",Dist2[i]);
      printf("            <entry>counties</entry>\n");
      printf("            <entry align=\"center\">3584</entry>\n");
      printf("            <entry align=\"center\">3932</entry>\n");
      if ( strcmp( Dist2[i],"NWS") )
	printf("            <entry>D%s counties to deliver ICS-217 directly to the SEOC</entry>\n",Dist2[i]);
      else
	printf("            <entry>NWS offices to deliver ICS-217 directly to the SEOC</entry>\n");

      printf("          </row>\n");

      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:20</entry>\n",nHour[i]+1);
      printf("            <entry align=\"center\">%s</entry>\n",Dist1[i]);
      printf("            <entry>%s inbound</entry>\n",Net1[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F1a[i]);
      printf("            <entry align=\"center\">3584</entry>\n");
      printf("            <entry>%s sends a liaison to SEOC to deliver traffic</entry>\n",Net1[i]);
      printf("          </row>\n");

      printf("          <row>\n");
      printf("            <entry align=\"right\">%2d:40</entry>\n",nHour[i]+1);
      printf("            <entry align=\"center\">%s</entry>\n",Dist2[i]);
      printf("            <entry>%s inbound</entry>\n",Net2[i]);
      printf("            <entry align=\"center\">%s</entry>\n",F2a[i]);
      printf("            <entry align=\"center\">3584</entry>\n");
      //      if ( strcmp( Dist2[i],"NWS") )
        printf("            <entry>%s sends a liaison to SEOC to deliver traffic</entry>\n",Net2[i]);
      //      else
	//	printf("            <entry>%s sends a liaison to SEOC to deliver responses from NWS offices</entry>\n",Net2[i]);
      printf("          </row>\n");


    }
  printf("        </tbody>\n");
  printf("      </tgroup>\n");
  printf("    </table>\n");
  printf("  </para>\n");
  printf("</section>\n");

}
