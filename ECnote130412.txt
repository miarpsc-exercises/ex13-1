Section-wide Drill May 11

On May 11 we will have our Spring exercise.  I know this comes close
on the heels of the April 27 meeting, and we are getting into the time
of year when counties are holding their exercises, we have walks,
runs, races and various other events, so it is a busy time.

It has been some time since we have given the counties a chance to
connect directly with the State EOC, but when we have done that in the
past, there has been a lot of congestion.  To try to alleviate that,
we are going to break the exercise into two hour slots, with each
district only participating in one of those slots, as follows:

 District  Start Time
    1         0800
    2         1700
    3         1700
    5         0800
    6         1300
    7         1300
    8         1000
   NWS        1000

Within each two hour segment, at the beginning of the segment, the
responsible net will send a liaison to the SEOC to pick of traffic for
the district.  Then there will be a period in which the counties will
send their ICS-217 to the SEOC.  Finally, at the end of the segment,
the net will bring responses back to the SEOC.

The good news is that there is only two hours when you need to be
operating, so if you have other things you want to test there is
plenty of time.  The bad news is that the particular two hour slot
might not be the most convenient, and it will be busy during that
short time.

Each county should already have an up to date 217 available as it
provides information needed for the 205 in case of an incident.
Normally, if we were to transmit this information we would likely
transmit the 205, but we should all be familiar with the 217.

Because the time is fairly compressed, planning is critical.  You will
need to figure out exactly what you plan to accomplish and who will be
doing what ahead of time.  DECs and Net Managers will need to work
especially closely to ensure they are able to carry out their tasks.

This will be simply a functional drill.  There will be no scenario, no
surprise injects.  We will simply be practicing our skills in moving
information likely to be needed in the event of an incident.

I would also like to see counties and nets keep the status of their
various stations up to date in the MI CIMS.  There is a training
session at the SEOC on April 24 for those folks, especially Net
Managers, who need it, but if you hope to get in you will need to sign
up on MiTrain very soon.  It is also the last chance before the first
D.C. Cook drill, so it will likely fill up fast.  For most folks their
Emergency Manager or MSP District Coordinator can arrange local
training. 

A document with more detail, including precise schedules and
frequencies, will be posted in the Library within a few days.

73 de WB8RCR
