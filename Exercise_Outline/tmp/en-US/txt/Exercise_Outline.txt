Product SiteDocumentation Site

Ex13-1



Exercise Outline
================


Overview of the Spring 2013 Section Exercise
--------------------------------------------


[IMAGE]


John McDonough

American Radio Relay League Michigan Sectionwb8rcr@arrl.net



Legal Notice
============

Copyright © 2013 The Michigan Section of the American Radio Relay League.
This material may only be distributed subject to the terms and conditions
set forth in the Open Publication License, V1.0, (the latest version is
presently available at http://www.opencontent.org/openpub/). ARES and
ARPSC are trademarks of the American Radio Relay League. All other
trademarks and copyrights referred to are the property of their
respective owners.

Abstract

Each Spring the Michigan Section holds an exercise for ARES and NTS to
supplement the annual SET held each October. This document provides
information necessary to participants in the 2013 exercise.

------------------------------------------------------------------------


1. Introduction
---------------

Twice each year the Michigan Section holds Section-wide exercises. These
exercises are designed to test needed communications skills. In recent
exercises, the emphasis has been on developing the ARES-NTS interface,
and working with the various FEMA forms. The Fall exercise is the annual
ARRL SET. This is always held on the first Saturday in October. This is
typically a fairly in-depth exercise. The exercise attempts to provide
opportunities for the various jurisdictions to interface with served
agencies. Although the Section-wide portion of the exercise is rarely a
full-scale exercise, the opportunity is there for the local jurisdictions
to develop their own FSE. The Spring exercise is usually more focused.
Typically, the exercise exploits some particular skill. Generally the
effort required of local jurisdictions is minimal, in order to give those
jurisdictions the opportunity to develop activities to practice locally
required skills. The Spring 2013 exercise will also be a very simple
exercise, focused on a few skills. Unlike many recent exercises, there is
no scenario, no surprise injects, no evaluators. Jurisdictions will
follow a pre-planned script designed to cause each jurisdiction to
execute specific tasks. Evaluation will be as observed from the SEOC, as
well as input from ECs.


2. Rationale
------------

There are four expectations from this exercise:

  1.  transmit several ICS forms

  2.  interface with the assigned NTS net

  3.  communicate directly with the SEOC

  4.  exploit the new MI CIMS


2.1. ICS Forms

The ICS-205 and the ICS-205a are forms detailing the communications
strategy for an incident. While most ICS forms we encounter are likely to
be prepared by served agencies, these are forms with which we should be
familiar. In addition, the ICS-217 is a form which can provide input for
the ICS-205. As such, Emergency Coordinators and Net Managers should be
familiar with this form as well.


2.2. Interface with the assigned NTS net

The NTS-ARES interface is critical for several reasons. First, NTS
operators are highly skilled at dealing with record traffic. While ARES
ops regularly transmit tactical messages, the opportunity to deal with
record traffic is less common. Using NTS allows ARES operators to focus
on more tactical needs while utilizing the greater skill of NTS ops for
record traffic. Secondly, longer haul traffic requires dealing with
unpredictable HF circuits and often congestion on frequency. Relieving
the ARES operators of these concerns gives them more time to focus on
local needs. Thirdly, when many counties are activated, the station at
the SEOC can get quite congested. Limiting the stations contacting the
SEOC helps alleviate this congestion. In addition, the more practiced NTS
operators take considerably less time to transmit and receive traffic
than less skilled operators.


2.3. Communicate directly with the SEOC

In recent years, exercises have focused on the NTS-ARES interface. Most
county EOCs have the capability to communicate directly with the SEOC,
however, having all counties attempt to contact the SEOC at the same time
is problematic. Nevertheless, counties need an opportunity to test their
direct circuits from time to time. This exercise will afford that
opportunity by breaking down the exercise into individual time slots for
each District.


2.4. Exploit the new MI CIMS

Recently the state has implemented a new Critical Incident Management
System, MI CIMS, often referred to as "WebEOC". This new system includes
features specific to amateur radio, and a large fraction of ARES and NTS
leadership now has access to this tool. This exercise will afford us an
opportunity to use this tool with some level of intensity, gaining more
practice and perhaps understanding the strengths and limitation.


3. Exercise Conduct
-------------------

The exercise will take place on Saturday, May 11, 2013. The exercise will
be broken into four, two-hour periods. Each District will be assigned a
specific period in which to operate.


3.1. Exercise Schedule

Each period will be similar. At the beginning of the period, the assigned
net will send a liaison to the SEOC to pick up traffic for the counties.
There are two nets and two districts per two-hour slot, so one net will
connect with the SEOC for the first 10 minutes, the second net for the
second 10 minute period. Then the first District will have 30 minutes in
which the individual counties will send their ICS-217 to the SEOC. Then
the second District. Finally, the first net will again send a liaison to
the SEOC, this time to deliver responses from the counties. This period
is 20 minutes. Finally the second net will deliver traffic to the SEOC.
The first District will have the time from 10 minutes into the segment
until 20 minutes into the segment, and again from 50 minutes in until 80
minutes in, during which they can get the traffic from their net,
formulate the response, and return the responses to their net.


Time is tight
-------------

Net Managers and DECs will need to carefully pre-plan their time as there
will not be a lot of time to get their work done. The second district in
the segment will have the time from 20 minutes in until 50 minutes into
the segment, and again from 80 minutes to 100 minutes to deliver their
responses to their net. The detailed schedule for the exercise is shown
in the following table:

Table 1. Exercise Schedule

Time

Dist

Net

Freq1

Freq2

What

8:00

1

QMN Outbound

3563

7064

QMN to send a liaison to SEOC, take traffic

8:10

5

MACS Outbound

3932

7232

MACS to send a liaison to SEOC, take traffic

8:20

1

counties

3584

3932

D1 counties to deliver ICS-217 directly to the SEOC

8:50

5

counties

3584

3932

D5 counties to deliver ICS-217 directly to the SEOC

9:20

1

QMN inbound

3563

3584

QMN sends a liaison to SEOC to deliver traffic

9:40

5

MACS inbound

3932

3584

MACS sends a liaison to SEOC to deliver traffic

10:00

8

UP Outbound

3932

7232

UP to send a liaison to SEOC, take traffic

10:10

NWS

GLETN Outbound

3932

7232

GLETN to send a liaison to SEOC, take traffic

10:20

8

counties

3584

3932

D8 counties to deliver ICS-217 directly to the SEOC

10:50

NWS

counties

3584

3932

NWS offices to deliver ICS-217 directly to the SEOC

11:20

8

UP inbound

3932

3584

UP sends a liaison to SEOC to deliver traffic

11:40

NWS

GLETN inbound

3932

3584

GLETN sends a liaison to SEOC to deliver traffic

13:00

6

MVTN Outbound

145470

3584

MVTN to send a liaison to SEOC, take traffic

13:10

7

MIDTN Outbound

3584

7045

MIDTN to send a liaison to SEOC, take traffic

13:20

6

counties

3584

3932

D6 counties to deliver ICS-217 directly to the SEOC

13:50

7

counties

3584

3932

D7 counties to deliver ICS-217 directly to the SEOC

14:20

6

MVTN inbound

145470

3584

MVTN sends a liaison to SEOC to deliver traffic

14:40

7

MIDTN inbound

3584

3584

MIDTN sends a liaison to SEOC to deliver traffic

15:00

2

SEMTN Outbound

147280

3584

SEMTN to send a liaison to SEOC, take traffic

15:10

3

MITN Outbound

3932

7232

MITN to send a liaison to SEOC, take traffic

15:20

2

counties

3584

3932

D2 counties to deliver ICS-217 directly to the SEOC

15:50

3

counties

3584

3932

D3 counties to deliver ICS-217 directly to the SEOC

16:20

2

SEMTN inbound

147280

3584

SEMTN sends a liaison to SEOC to deliver traffic

16:40

3

MITN inbound

3932

3584

MITN sends a liaison to SEOC to deliver traffic


Table only identifies SEOC schedule
-----------------------------------

Note that the above table identifies only contacts with the SEOC. Each
District and Net will need to work together to determine the optimum
schedule for within-District activities.


3.2. Exercise Tasks

Each jurisdiction will need to execute a number of tasks during the
exercise. Some may take considerable creativity on the part of the
players. The exercise relies heavily on ICS forms.


ICS forms available on MI CIMS
------------------------------

Most relevant ICS forms may be found at Reference Info -> ICS Forms. The
ICS-217 may be found at Reference Info -> File Library -> ICS Forms.

3.2.1. Tasks for NTS

Each net will need to send a liaison to the SEOC at the beginning of the
time segment to pick up radiogram traffic from the SEOC bound for the
served counties. The net will then need to deliver the traffic to the
counties. Since there is not a lot of time for this activity, it is
imperative that the Net Manager and DEC have worked out ahead of time the
times, modes and frequencies on which this will be done. The individual
counties may attend a meeting of the net (on a frequency not in use by
the SEOC), or the DEC may take all the traffic from the net and deliver
it to the counties. The DEC and Net Manager may decide that some other
plan is appropriate. Prior to the exercise, the District might enlist the
assistance of the net in determining how to effectively move the
District's ICS-217 forms to the SEOC. The net must then collect responses
from the counties, again, the Net Manager and DEC must determine how best
to do this for their jurisdiction. Finally, the net must send a liaison
to the SEOC to deliver the county responses. The Net Manager should
consider all available assets. For example just because it is an HF phone
net, there is no law preventing the liaison station from contacting the
SEOC via CW, MT-63, or even linked repeaters. Net Managers should
maintain their station status in the MI CIMS, and also status of other
key stations if those stations do not have access to the MI CIMS. If the
Net Manager cannot access the CIMS, he/she should appoint a member
station to provide those updates. All traffic should be logged in the MI
CIMS.

3.2.2. Tasks for ARES

Each county will collect traffic from the SEOC. The DEC and Net Manager
will have had to arrange just how that is to be done. It may be that each
county will send a representative to a meeting of the net, or perhaps the
DEC will assign a station to collect traffic for the counties. The
traffic from the SEOC will request information from the counties.
Counties will need to prepare a response and return that response to
their assigned NTS net for delivery to the SEOC. Each district has an
assigned slot for meeting with the SEOC. Counties should send their
ICS-217 to the SEOC during this time slot. Emergency Coordinators should
consider whether to arrange other tasks to practice skills needed in
their jurisdiction. Any significant activities taken during the event
should be recorded in the Activity Log. For EOC activation and
deactivation, the Post to SEOC Significant Events button should be
checked. Each EC should enter a record in AuxComm Station Status for each
station active within their jurisdiction. At least for the EOC, the
Situation Update should be completed in some detail. All record traffic
should be recorded in the AuxComm Message Log.


4. Modes and Frequencies
------------------------

Table 1, “Exercise Schedule” shows a number of different frequencies.
These represent frequencies that the SEOC will be monitoring during the
specified time slot. The mode depends on the frequency.

Table 2. Exercise Frequencies

Frequency

Notes

3563

Michigan emergency frequency, CW

3584

(3583 dial frequency) Michigan digital frequency. Use MT-63 1K unless
there are thunderstorms in your area or in the area of the SEOC. In that
case switch to Olivia 8/500. Check with your NWS DEC for weather
conditions.

3932

This is the Michigan emergency frequency, SSB.

5371.5

(Dial frequency). In the event of a low F0F2 coupled with a high K,
stations should be prepared to use this frequency. If the frequency is
required and occupied, check other channels. this frequency may only be
used for USB and PSK-31.

7063

This is the backup Michigan Emergency Frequency, CW. Use this frequency
if the F2 layer critical frequency, F0F2, exceeds 8 MHz or if the
planetary K index exceeds 4.

7232

This is the backup Michigan Emergency Frequency, SSB. Use this frequency
if the F2 layer critical frequency, F0F2, exceeds 8 MHz or if the
planetary K index exceeds 3.

145470

This is the Lansing IRA FM repeater. Stations should use their nearest
IRA repeater. Lansing PL 100. For other frequencies and PL requirements,
visit http://www.w8hvg.org/repeatermap.htm. Net Managers and District
Emergency Coordinators should plan relays or other alternatives in case
of repeater problems.

147280

This is the Lansing CMEN FM repeater. Stations should use their nearest
CMEN repeater. Lansing PL 100. For other frequencies and PL requirements,
visit http://cmen.us/repeaters/. Net Managers and District Emergency
Coordinators should plan relays or other alternatives in case of repeater
problems.


Use the appropriate mode
------------------------

Nets and counties should use the most appropriate mode and frequency for
the traffic to be handled. There is no law preventing, for example, an HF
net from sending a liaison to the SEOC on VHF. Obviously, modes and
frequencies must be negotiated between both ends of the circuit.


5. ICS Forms
------------

Since we would expect most record traffic to involve ICS forms, the
exercise relies heavily on these forms. There are many versions of ICS
forms on the web. For those forms that are supported by flmsg, the
versions in flmsg should be used. Many other forms are available in
MI-CIMS. The exercise exploits those forms we are most likely to need
ourselves, the 205, 205a, 217 and 217a. Emergency Coordinators and Net
Managers should familiarize themselves with all forms that are likely to
be used by their jurisdictions, however.


Only use relevant fields
------------------------

The various ICS forms are intended to be fairly generic, and as a result,
there are many fields that are simply not relevant. Only use those fields
that make sense. There are many examples on the web of filled out forms,
especially those relevant to amateur radio. All members should be
encouraged to review these forms as various jurisdictions tend to have
different needs and find good ways to exploit these resources.


6. MI CIMS Entries
------------------

All Net Managers and Emergency Coordinators should become familiar with
the Michigan Critical Incident Management System, MI CIMS. This tool
provides for good documentation of an incident as well as situational
awareness. During actual incidents, this is the primary way State
Agencies stay aware of what each other are doing, including us. For this
exercise, stations should log on to the incident
TRN-2013-05-11-NTS/ARES-Functional-Drill and add all records to that
incident. When first activated, ECs and NMs should first consider filling
out an ICS-202, the incident objectives. If the SEOC has been activated,
the SEOC AuxComm objectives should be reviewed. This can be found by
selecting SEOC Situation Report - Agency List, then selecting AuxComm
from the agency list. Besides communicating your objectives to other
agencies, the ICS-202 serves the additional purpose of giving the
Emergency Coordinator or Net Manager a few minutes to consider where the
focus should reside. It can also serve as a valuable communications tool
to your own staff. Incident Objectives Incident Objectives

Figure 1. Incident Objectives


Then both the counties and the nets should prepare a record in the
AuxComm Station Status for each facility to be manned. For nets, these
"facilities" tend to be roles, such as Net Control, District Liaison,
etc. Station Status Entry Station Status Entry

Figure 2. Station Status Entry


Next, scroll down the facility board to Operator Information and assign
operators to the facility/role. Note that operators may be assigned for
future shifts, and when the shifts occur, the operator information may be
rearranged with the Up and Down buttons. Operator Information Operator
Information

Figure 3. Operator Information


All record traffic must be recorded in the AuxComm Message Log. For
traffic passing through the nets, the Net Manager should feel responsible
for these entries, however, depending on how traffic is routed within the
district, that may not be possible. Message Log Message Log Entry

Figure 4. Message Log


The message originator can log the message when first placed on the air,
then the delivering station can record delivery with the Update button on
the message list. Message List Message List

Figure 5. Message List


It is important during an incident to share your status, so you must be
capable of doing that.


Assign Record Keeper
--------------------

Not everyone has yet gotten access to the MI CIMS, so it is important
that you identify someone who will be responsible for keeping your status
up to date in MI CIMS. If you cannot find someone from your jurisdiction
to keep MI CIMS updated for you, identify someone from another
jurisdiction. Clearly, this must be planned well ahead of time. You may
also find it useful to keep notes in the Activity Log for future
reference. Activity Log Activity Log Entry

Figure 6. Activity Log


When you update an activity log record, your entry is shown within the
record with a new timestamp. You may update a record many times, and you
may have as many records as you like. The Activity Log entry is private,
unless you check one of the three buttons at the bottom, "Post to SEOC
Significant Events", "Post to Statewide Significant Events", or "REP". In
that case your note will be shown in the appropriate "Significant Events"
board. If a record so marked is updated, the update also shows in the "Significant
Events" board. Activity Log Display showing updates Activity Log Display

Figure 7. Activity Log Display showing updates


A. Revision History
-------------------

Revision History

Revision 1.0

Tue Apr 23 2013

John McDonough

Remove draft tag

Revision 0.4

Tue Apr 23 2013

John McDonough

Spell check

Additional notes

MI CIMS screenshots

Assorted Cleanup

Revision 0.3

Wed Apr 17 2013

John McDonough

Abstract

Revision 0.2

Tue Apr 16 2013

John McDonough

Rationale, Modes

Revision 0.1

Fri Apr 12 2013

John McDonough

Schedule

Revision 0.0

Fri Apr 12 2013

John McDonough

Initial creation of book by publcan
